require('dotenv').config();
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const { KEY, PORT } = process.env;

let CURRENT_TIMESTAMP = 0;
let CURRENT_STATE = 0; // 0=stop, 1=play, 2=pause
let CURRENT_FILE = null;
let CURRENT_SUB_FILE = null;
let CONNECTED_USERS = 0;

server.listen(PORT);

app.use('/', express.static(__dirname + '/static'));

function emitState() {
  console.log('emitting state');
  io.emit('status.updated', {
    ts: CURRENT_TIMESTAMP,
    state: CURRENT_STATE,
    file: CURRENT_FILE,
    subFile: CURRENT_SUB_FILE,
    users: CONNECTED_USERS
  });
}
//setInterval(emitState, 10000);

var hostSocket = null;

io.on('connection', function (socket) {
  CONNECTED_USERS++;
  console.log(`new client; CONNECTED_USERS=${CONNECTED_USERS}`);
  socket.emit('status.updated', {
    ts: CURRENT_TIMESTAMP,
    state: CURRENT_STATE,
    file: CURRENT_FILE,
    subFile: CURRENT_SUB_FILE,
    users: CONNECTED_USERS
  });

  socket.on('auth.authenticate', function (data) {
    if (typeof data === 'object' && data !== null) {
      if (data.key === KEY) {
        console.log('admin authenticated');
        socket.on('status.update', data => {
          if (typeof data === 'object' && data !== null) {
            if (typeof data.ts === 'number') {
              CURRENT_TIMESTAMP = data.ts;
            }
            if (typeof data.state === 'number') {
              CURRENT_STATE = data.state;
            }
            if (typeof data.file === 'string') {
              CURRENT_FILE = data.file || null;
            }
            if (typeof data.subFile === 'string') {
              CURRENT_SUB_FILE = data.subFile || null;
            }
            console.log('state updated');
            emitState();
          }
        });

        socket.on('host.become', () => {
          if (hostSocket !== null) {
            hostSocket.emit('host.lost');
          }
          hostSocket = socket;
          socket.emit('host.became');
        })

        socket.on('disconnect', () => {
          if (hostSocket === socket) {
            hostSocket = null;
            if (CURRENT_STATE === 1) {
              CURRENT_STATE = 2;
              emitState();
              console.log('host disconnect, paused media');
            } else {
              console.log('host disconnect');
            }
          }
        })
        socket.emit('auth.authenticated', true);
      }
    } else {
      console.log('failed authentication attempt');
      socket.disconnect();
    }
  });

  socket.on('disconnect', () => {
    CONNECTED_USERS--;
    console.log('disconnect');
  });
});
